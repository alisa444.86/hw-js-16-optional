/*
* Рекурсия - это вызов функцией самой себя.
* На практике используется для некоторых вычислений (факториал, возведение в степень), и
* для работы со вложенными обектами или массивами. Если объект имеет вложенные объекты,
* функция может вызываться на родительском объекте, на его дочерних эелементах, или
* на объектах второго порядка вложенности (дочерних объектах дочерних элементов)
* */

let num = +prompt('Enter a number', ``);
while (!num ||  Number.isNaN(num)) {
    num = +prompt('You didn`t enter a valid number. Try again, please!', `${num}`);
}

console.log(factorial(num));

function factorial(number) {
    if (number !== 1) {
        return number * factorial(number - 1)
    } else {return 1}
}

